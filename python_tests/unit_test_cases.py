# 15. Cover your basics task functions with unit tests. (Possible to use `setUp()` while testing)

import unittest

from pythonbasics import myfunc                     #   t01funcarg2.py
from pythonbasics import summ, multiply             #   t02funcmult2.py

class myfunc_test(unittest.TestCase):

    def test_positive(self):
        self.assertEquals(myfunc(self.myinput), 'Hello, Max!')

    def test_negative(self):
        with self.assertRaises(TypeError):
            myfunc(self.myfailinput)

    def test_failed(self):
        self.assertEqual(myfunc(''), '')

    @classmethod
    def setUpClass(cls):
        cls.myinput = 'Max'
        cls.myfailinput = ''
        print("before class")

    def tearDown(self):
        print("after test")

    def setUp(self):
        print("setup before each")

    @classmethod
    def tearDownClass(cls):
        print("after class")

class summ_test(unittest.TestCase):

    def test_positive(self):
        self.assertEquals(summ(self.myinput), 5)

    def test_negative(self):
        with self.assertRaises(TypeError):
            summ(self.myfailinput)

    def test_failed(self):
        self.assertEqual(summ('a'), 'a')

    @classmethod
    def setUpClass(cls):
        cls.myinput = [2, 3]
        cls.myfailinput = 'a'
        print("before class")

    def tearDown(self):
        print("after test")

    def setUp(self):
        print("setup before each")

    @classmethod
    def tearDownClass(cls):
        print("after class")

class multiply_test(unittest.TestCase):

    def test_positive(self):
        self.assertEquals(multiply(self.myinput), 6.0)

    def test_negative(self):
        with self.assertRaises(TypeError):
            multiply(self.myfailinput)

    def test_failed(self):
        self.assertEqual(multiply('a'), 'a')

    @classmethod
    def setUpClass(cls):
        cls.myinput = [2.0, 3.0]
        cls.myfailinput = 'a'
        print("before class")

    def tearDown(self):
        print("after test")

    def setUp(self):
        print("setup before each")

    @classmethod
    def tearDownClass(cls):
        print("after class")

if __name__ == '__main__':
    unittest.main()
