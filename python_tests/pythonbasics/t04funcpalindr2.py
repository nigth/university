# 04. Define a function `isPalindrome()` that recognizes palindromes (i.e. words that look 
# the same written backwards). For example, `isPalindrome("radar")` should return True.

def isPalindrome (arg):
    l = len(arg)
    result = ''
    for n in range(l, 0, -1) :
        result += arg[n-1]
    if arg == result:
        print ('This is palindrome: ', result)
    else:
        print ('This is not palindrome: ', result)

phrase = input('Please input something for palindrome checking:\n')

isPalindrome (phrase)







