# 05. Define a procedure `histogram()` that takes a list of integers and prints a histogram to the screen. For example, `histogram([4, 9, 7])` 
# should print the following: (usage some kind of sleep for better visualization)
# ****
# *********
# ******

from time import sleep

def histogram (*args):
    for value in args:
        for level in range(value):
            #print ('*'),
            #print ('*'.rstrip())
            print ('*', end='', flush=True)
            sleep (level/7)
        print (' ')

values = input('Please input integer values for histogram using spaces :\n')
nums = list(map(int,values.split()))

histogram (*nums)







