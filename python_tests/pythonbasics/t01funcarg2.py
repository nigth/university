# function version 2 - modified for testing
# 01. Define a function, that takes string as argument `arg` and prints `"Hello, %arg%!"`


def myfunc(arg):
    answer = "Hello, {0}!".format(arg)
    return answer


person = input('What is your name? \n')
name = myfunc(person)
print(name)
