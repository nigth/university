# 12. Write a ship battle game, which is similar to ex.8, except it takes 1 integer as an order 
# of matrix, randomly generates index (x, y) and checks user input (2 integers).
# hard task: Visualize the game.

import random

# You can change the symbols for game as you want:
ship = '#'
near = '.'
empty = ' '
firedship = 'X'
firedempty = '+'

def gettingorder (): # Gettting order of matrix from user
    order = input ('\nPlease input size of sea between 10 and 26 (by default 10): ')
    if order == '' or int(order) < 10 or int(order) > 26:
        order = 10
    order = int(order)
    return (order)

def seainit (arg):   # Initialize empty sea field
    field = [[empty for x in range(arg)] for y in range(arg)] 
    return(field)

def visualize ():    # Print visualised sea field
    print ('\n~~|', end = '', flush = True)
    for x in range (size):
        if x > 9:
            print (x, end = ' ', flush = True)
        else:
            print ('0'+str(x), end = ' ', flush = True)
    print (' ')
    for x in range (size+1):
        print ('---', end = '', flush = True)
    print (' ')
    for y in range (size):
        if y > 9:
            print(str(y)+'|', end = '', flush = True)
        else:
            print('0'+str(y)+ '|', end = '', flush = True)
        for x in range (size):
            if sea[x][y] == ship or sea[x][y] == near:
                print('',sea[x][y], end=' ', flush=True)    # COMMENT this for real game
                #print('',empty, end=' ', flush=True)      # UNcomment this for real game
            else:
                print('',sea[x][y], end=' ', flush=True) 
        print(' ')
    #print(' ')
    pass

def markaround (x, y, size):    # Marking as NEAR all around ship position
    if x>0 and y<(size-1):
        sea[x-1][y+1] = near
    if y<(size-1) and sea[x][y+1] != ship:
        sea[x][y+1] = near
    if x<(size-1) and y<(size-1):
        sea[x+1][y+1] = near
    if x>0 and sea[x-1][y] != ship:
        sea[x-1][y] = near
    if x<(size-1) and sea[x+1][y] != ship:
        sea[x+1][y] = near
    if x>0 and y>0:
        sea[x-1][y-1] = near
    if y>0 and sea[x][y-1] != ship:
        sea[x][y-1] = near
    if x<(size-1) and y>0:
        sea[x+1][y-1] = near            
    pass

def carrier ():      # Set up 1 carrier ship, long 4
    shipleng = 4                # ship length
    dx = random.randint (0,1)   # setting the horizontal delta
    dy = 1 - dx                 # set vertical delta in oppozite to horizontal
    x0 = random.randint (0, (size-1 - shipleng*dx)) # randomize X start point
    y0 = random.randint (0, (size-1 - shipleng*dy)) # randomize Y start point
    for pos in range (shipleng):
        x = x0+dx*pos
        y = y0+dy*pos
        sea[x][y] = ship
        markaround (x, y, size)        
    pass

def cruiser ():      # Set up 2 cruiser ships, long 3
    shipleng = 3                # ship length
    for amount in range (2):
        ok = False
        while ok == False:
            dx = random.randint (0,1)   # setting the horizontal delta
            dy = 1 - dx                 # set vertical delta in oppozite to horizontal             
            x0 = random.randint (0, (size-1 - shipleng*dx)) # randomize X start point
            y0 = random.randint (0, (size-1 - shipleng*dy)) # randomize Y start point
            if sea[x0][y0] == empty:
                ok = True
                for pos in range (shipleng):
                    x = x0+dx*pos
                    y = y0+dy*pos
                    if sea[x][y] == empty:
                        ok = True
                    else:
                        ok = False  
            else:
                ok = False

        for pos in range (shipleng):
            x = x0+dx*pos
            y = y0+dy*pos
            sea[x][y] = ship
            markaround (x, y, size)          
    pass

def submarine ():    # Set up 3 submarine ships, long 2
    shipleng = 2                # ship length
    for amount in range (3):
        ok = False
        while ok == False:
            dx = random.randint (0,1)   # setting the horizontal delta
            dy = 1 - dx                 # set vertical delta in oppozite to horizontal             
            x0 = random.randint (0, (size-1 - shipleng*dx)) # randomize X start point
            y0 = random.randint (0, (size-1 - shipleng*dy)) # randomize Y start point
            if sea[x0][y0] == empty:
                ok = True
                for pos in range (shipleng):
                    x = x0+dx*pos
                    y = y0+dy*pos
                    if sea[x][y] == empty:
                        ok = True
                    else:
                        ok = False  
            else:
                ok = False
        for pos in range (shipleng):
            x = x0+dx*pos
            y = y0+dy*pos
            sea[x][y] = ship
            markaround (x, y, size)           
    pass

def destroyer ():    # Set up 4 destroyer ships, long 1
    #shipleng = 1                # ship length
    for amount in range (4):           
        ok = False
        while ok == False:
            x = random.randint (0, (size-1)) # randomize X start point
            y = random.randint (0, (size-1)) # randomize Y start point
            if sea[x][y] == empty:
                ok = True
        sea[x][y] = ship
        markaround (x, y, size)
    pass

def guess (*args):    # Check the ship in (x, y) and modify battle field if shoot
    a = args[0]
    b = args[1]
    if a in range (size) and b in range (size):
        if sea[a][b] == ship or sea[a][b] == firedship:
            if sea[a][b] == ship:
                sea[a][b] = firedship
            if ship in str(sea):
                phrase = 'You shot at the ship!! Continue:'
            else:
                phrase = 'Congratulations! You won!!'
        else:
            phrase = 'You are in an empty place! Try again:'
            sea[a][b] = firedempty
    else:
        phrase = 'Index is out of range, try again.'
    return (phrase)
    
size = gettingorder()   # Ask player for an order of matrix for game
sea = seainit (size)    # Initialise sea field by empty characters

print ('\nThe new clean battle field sea:')
visualize() 

# Create ships and organize it on the sea
carrier ()
cruiser ()
submarine ()
destroyer ()

# Start test checking. For real game - please comment next 2 lines:
print ('\nThe sea fulfilled by ships:')
visualize() 
# The end of test checking. For real game - please comment previous 2 lines.

print ('Please, make your game! Guess X and Y coordinates of the ships (using space) or Q to Quit:')
while ship in str(sea):
    try:
        coords = input()
        if coords == 'q' or coords == 'Q':
            print ('Good bye!')
            break
        result = guess (*list(map(int,coords.split())))
        visualize() 
        print (result)
    except:
        print ('Incorrect data, please check it:')
