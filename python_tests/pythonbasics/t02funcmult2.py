# function version 2 - modified for testing
# 02. Define a function `sum()` and a function `multiply()` # that sums and multiplies 
# (respectively) all the numbers in a list of numbers. # For example, `sum([1, 2, 3, 4])` 
# should return 10, # and `multiply([1, 2, 3, 4])` should return 24.


def summ(*args):
    result = 0
    for i in args:
        result += i
    return result


def multiply(*args):
    result = 1
    for i in args:
        result *= i
    return result


numbers = input("Please input some numbers using spaces:\n")

if "." in numbers:
    nums = list(map(float, numbers.split()))
else:
    nums = list(map(int, numbers.split()))

s = summ(*nums)
m = multiply(*nums)
print("The summary is: ", s)
print("The multiply is: ", m)
