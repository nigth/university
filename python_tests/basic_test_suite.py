# 15. Cover your basics task functions with unit tests. (Possible to use `setUp()` while testing)

import unittest
from unit_test_cases import myfunc_test                         #   t01funcarg2.py
from unit_test_cases import summ_test, multiply_test            #   t01funcarg2.py

newSuite = unittest.TestSuite()

newSuite.addTest(unittest.makeSuite(myfunc_test))

newSuite.addTest(unittest.makeSuite(summ_test))

newSuite.addTest(unittest.makeSuite(multiply_test))

if __name__ == '__main__':
    unittest.main()
