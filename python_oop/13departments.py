#13. Create structure for department:
#   * There are 3 types of employee: developer, designer and manager
#* Each employee has: first name, second name, salary, experiance (years)
#   * Each designer, developer or manager can have higher managers
#   * Each designer has effectivness coefficient(0-1)
#   * Each manager has team of developers and designers.
#* Department should have list of managers(which have their own teams)
#* Department should be able to give salary (for each employee message:
# `"@firstName@ @secondName@: got salary: @salaryValue@"`)
#   * Each employee gets the salary, defined in field Salary.
# If experiance of employee is > 2 years, he gets bonus + 200$,
# if experiance is > 5 years, he gets salary*1.2 + bonus 500
#* Each designer gets the salary = salary*effCoeff
#* Each manager gets salary +
#    1. 200$ if his team has >5 members
#    2. 300$ if his team has >10 members
#    3. salary*1.1 if more than half of team members are developers.
#* Redefine string representation for employee as follows: `"@firstName@
# @secondName@, manager:@manager secondName@, experiance:@experiance@"`
#------------------------ S O L U T I O N -------------------------------
#1. employ: parent class, 4 attribs (fname, sname, salary, exper), 2 methods (represent, getsalary)
#2. devel: child employ class, +1 attrib (highmanag), 2 methods (represent, getsalary)
#3. desing: child employ class, +2 attribs (effect,highmanag), 2 methods (represent, getsalary)
#4. manag: child employ class, +2 attribs (team,highmanag), 2 methods (represent, getsalary)
#5. depart: independent class, 2 attribs(depname, managlist), 2 methods(represent, givesalary)
#-------------------------------------------------------------------------
class employ (object):                            # Define class for Employees
    def __init__(self, fname, sname, salary, exper):
        self.fname = fname
        self.sname = sname  
        self.salary = salary
        self.exper = exper
        print ('Created Employee: {0}'.format(self.fname))
    def __str__(self):
       return '\nEmployee: {} {} {} {}'.format(self.fname, self.sname, self.salary, self.exper)
 
    def represent (self):
        print (self.fname, self.sname, ', experience:', self.exper)
    
    def getsalary (self):
        print ('Getting Employee salary')

class Manager (employ):                             # Inherite class Employee to Managers
    def __init__(self, fname, sname, salary, exper, team, highmanag):
        employ.__init__(self, fname, sname, salary, exper)
        self.team = team
        self.highmanag = highmanag
        print('Created Manager: {0}'.format(self.sname))
    def __str__(self):
        return '\nManager: {} {} {} {} {} \nHis team: {}'.format(self.fname, self.sname, self.salary, self.exper, self.highmanag, *self.team)

    def represent(self):
        print (self.fname, self.sname, ', manager:', self.highmanag, ', experience:', self.exper)

    def getsalary(self):
        print ('Getting Manager salary')
        
class Developer (employ):                             # Inherite class of Employees to Developers
    def __init__(self, fname, sname, salary, exper, highmanag):
        employ.__init__(self, fname, sname, salary, exper)
        self.highmanag = highmanag
        print('Created Developer: {0}'.format(self.sname))
    def __str__(self):
        return '\nDeveloper: {} {} {} {} {}'.format(self.fname, self.sname, self.salary, self.exper, self.highmanag)

    def represent (self):
        print (self.fname, self.sname, ', manager:', self.highmanag, ', experience:', self.exper)

    def getsalary(self):
        print ('Getting Developer salary')

class Designer (employ):                            # Inherite class of Employees to Designers
    def __init__(self, fname, sname, salary, exper, effect, highmanag):
        employ.__init__(self, fname, sname, salary, exper)
        self.effect = effect
        self.highmanag = highmanag
        print('Created Designer: {0}'.format(self.sname))
    def __str__(self):
        return '\nDesigner: {} {} {} {} {} {}'.format(self.fname, self.sname, self.salary, self.exper, self.effect, self.highmanag)

    def represent(self):
        print (self.fname, self.sname, ', manager:', self.highmanag, ', experience:', self.exper)

    def getsalary(self):
        print ('Getting Designer salary')

class depart (object):                            # Define class for Department
    def __init__(self, depname, managlist):
        self.depname = depname
        self.managlist = managlist
        print('Created Departament: {0}'.format(self.depname))
    def __str__(self):
        return '\ndepname: {}, managlist: {}'.format(self.depname, *self.managlist)

    def represent(self):
        print ('Department representation:', self.depname,'\nMgrs:', *self.managlist)
 
    def givesalary(self, people):
        print ('\nCalculating salary for ', people.fname, people.sname)
        money = people.salary
        if people.__class__.__name__ == 'Designer':
            print ('this designer has coefficient = ', people.effect)
            money *= people.effect
        elif people.__class__.__name__ == 'Manager':
            devs = 0
            size = 0
            for i in people.team:
                size += 1
                if i.__class__.__name__ == 'Developer':
                    devs +=1
            if devs >= size/2:
                money *= 1.1
            else:
                money = people.salary
            if size > 10:
                money += 300
            elif size >5 and size<=10:
                money += 200
            print ('This manager has team size:', size, 'and developers: ', devs)
        print ('1. Basic salary with coefficients = ', money)
        if people.exper > 5:
            expbonus = people.salary * 0.2 + 500
            print ('2. He has expbonus for more 5 years = ', expbonus)
        elif people.exper > 2 and people.exper <=5:
            expbonus = 200
            print ('2. He has expbonus for more 2 years = ', expbonus)
        else:
            expbonus = 0
            print ('2. He hasn`t exp bonus.')
        money += expbonus
        print (people.fname, people.sname, ': got salary:', money)

#Initialization array of employees:
dev1 = Developer('Linus', 'Torvalds', 2500, 3, '')
dev2 = Developer('Bill', 'Gates', 1500, 6, '') 
dev3 = Developer('Kevin', 'Mitnik', 500, 1, '') 
des1 = Designer('Artem', 'Lebedev', 1000, 4, 0.9, '')
des2 = Designer('Pablo', 'Picasso', 2500, 9, 0.7, '')
des3 = Designer('Salvador', 'Dali', 2000, 2, 0.7, '')
man1 = Manager('Li', 'Yakokka', 3000, 7, [dev1, dev2, des3], '')
man2 = Manager('Masasi', 'Yamamoto', 4500, 8, [des1, des2, dev3], '')
man3 = Manager('Larry','Page',4000,5,[man1,man2],'none') # General director, he hasn't high manager
dev1.highmanag = dev2.highmanag = des3.highmanag = man1.sname
des1.highmanag = des2.highmanag = dev3.highmanag = man2.sname
man1.highmanag = man2.highmanag = man3.sname
dep = depart ('STARTUP', [man1, man2, man3])

# Departament structure printing
print ('\nThe ~{}~ department employees:'.format(dep.depname))
for m in dep.managlist:
    print ('\nManager: {} {} and his team:'.format(m.fname, m.sname))
    for e in m.team:
        print (e.__class__.__name__)
        e.represent()

# Salary workflow process
print ('\nNow it`s salary time!')
for m in dep.managlist:
    #print ('\nManager: {} {} and his team:'.format(m.fname, m.sname))
    for e in m.team:
        #print (e.__class__.__name__)
        #e.represent()
        dep.givesalary(e)


