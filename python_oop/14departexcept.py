#14. Modify your department:
# - Change giving salary method: if there is not Employee in manager's team, 
# raise SalaryGivingError(You should create the Error class).
# - Add method add_to_team(array) to manager class, which adds employees to team. 
# If there are not Employees in array, raise your own NotEmployeeException and 
# if Employee is Manager, reise WrongEmployeeRoleError with parameter "second_name". 
# String representation of error should return message "Employee @second_name@ has unexpected role!"
# - In Department class create method add_team_members(manager, array) which tries to add array 
# of Employees to manager team. Handle the exceptions, raised in manager's add_to_team method.

class employ (object):                            # Define class for Employees
    def __init__(self, fname, sname, salary, exper):
        self.fname = fname
        self.sname = sname  
        self.salary = salary
        self.exper = exper
        print ('Created Employee: {0}'.format(self.fname))
 
    def represent (self):
        print (self.fname, self.sname, ', experience:', self.exper)
    
    def getsalary (self):
        print ('Getting Employee salary')

class manager (employ):                             # Inherite class Employee to Managers
    def __init__(self, fname, sname, salary, exper, team, highmanag):
        employ.__init__(self, fname, sname, salary, exper)
        self.team = team
        self.highmanag = highmanag
        print('Created manager: {0}'.format(self.sname))

    def represent(self):
        print (self.fname, self.sname, ', manager:', self.highmanag, ', experience:', self.exper)

    def getsalary(self):
        print ('Getting manager salary')

    def add_to_team (self, array):
        self.array = array
        if self.array == '' or self.array == []:
            mistake = error ('NotEmployeeException')
            print (mistake.errortype)
        else:
            for r in self.array:
                if r.__class__.__name__ == 'manager':
                    mistake = error ('WrongEmployeeRoleError')
                    print (mistake.errortype, r.sname)
                else:
                    print ('Adding', r.__class__.__name__, r.sname, 'to the ', self.sname, 'manager team')
                    self.team.append(r)
                    r.highmanag == self.sname
                    
                    
  
        
class developer (employ):                             # Inherite class of Employees to Developers
    def __init__(self, fname, sname, salary, exper, highmanag):
        employ.__init__(self, fname, sname, salary, exper)
        self.highmanag = highmanag
        print('Created developer: {0}'.format(self.sname))

    def represent (self):
        print (self.fname, self.sname, ', manager:', self.highmanag, ', experience:', self.exper)

    def getsalary(self):
        print ('Getting developer salary')

class designer (employ):                            # Inherite class of Employees to Designers
    def __init__(self, fname, sname, salary, exper, effect, highmanag):
        employ.__init__(self, fname, sname, salary, exper)
        self.effect = effect
        self.highmanag = highmanag
        print('Created designer: {0}'.format(self.sname))

    def represent(self):
        print (self.fname, self.sname, ', manager:', self.highmanag, ', experience:', self.exper)

    def getsalary(self):
        print ('Getting designer salary')

class department (object):                            # Define class for Department
    def __init__(self, depname, managlist):
        self.depname = depname
        self.managlist = managlist
        print('Created departament: {0}'.format(self.depname))

    def represent(self):
        print ('Department representation:', self.depname,'\nMgrs:', *self.managlist)

    def add_team_members (self, manager, array):
        self.manager = manager
        self.array = array
        try:
            manager.add_to_team (array)
        except:
            mistake = error ('Something wrong!')
            print (mistake.errortype, 'You should add team using ARRAY type!')
 
    def givesalary(self, people):
        #print ('\nCalculating salary for ', people.fname, people.sname)
        size = 0
        money = people.salary
        if people.__class__.__name__ == 'designer':
            #print ('this designer has coefficient = ', people.effect)
            money *= people.effect
        elif people.__class__.__name__ == 'manager':
            devs = 0
            for i in people.team:
                size += 1
                if i.__class__.__name__ == 'developer':
                    devs +=1
            if devs >= size/2:
                money *= 1.1
            if size == 0:
                mistake = error ('SalaryGivingError')
                print (mistake.errortype, people.fname, people.sname)
            elif size >5 and size<=10:
                money += 200
            elif size > 10:
                money += 300 

        #print ('This manager has team size:', size, 'and developers: ', devs)
        #print ('1. Basic salary with coefficients = ', money)
        if people.exper > 5:
            expbonus = people.salary * 0.2 + 500
            #print ('2. He has expbonus for more 5 years = ', expbonus)
        elif people.exper > 2 and people.exper <=5:
            expbonus = 200
            #print ('2. He has expbonus for more 2 years = ', expbonus)
        else:
            expbonus = 0
            #print ('2. He hasn`t exp bonus.')
        money += expbonus
        if people.__class__.__name__ == 'manager' and size == 0:
            money = 0
        print (people.fname, people.sname, ': got salary:', money)

class error (object):                               # Define class for Errors
    def __init__ (self, errortype):
        self.errortype = errortype
        if self.errortype == 'SalaryGivingError':
            self.errortype = 'Salary giving error! There is no employee in manager`s team:'
        elif self.errortype == 'WrongEmployeeRoleError':
            self.errortype = 'Wrong Employee Role Error! Employee is Manager:'
        elif self.errortype == 'NotEmployeeException':
            self.errortype = 'Not Employee Exception! There are no Employees in array to add!'
        else:
            #print (self.errortype)
            self.errortype = 'It is unexpected error!'
       
#Initialization array of employees:
print ('')
dev1 = developer('Linus', 'Torvalds', 2500, 3, '')
dev2 = developer('Bill', 'Gates', 1500, 6, '') 
dev3 = developer('Kevin', 'Mitnik', 500, 1, '') 
des1 = designer('Artem', 'Lebedev', 1000, 4, 0.9, '')
des2 = designer('Pablo', 'Picasso', 2500, 9, 0.7, '')
des3 = designer('Salvador', 'Dali', 2000, 2, 0.7, '')
man1 = manager('Li', 'Yakokka', 3000, 7, [], '')
man2 = manager('Masasi', 'Yamamoto', 4500, 8, [], '')
man3 = manager('Larry','Page',4000,5,[man1,man2],'none') # General director, he hasn't high manager
man4 = manager('Super','Man',7777,7,'','none') # This is error manager without team
man1.highmanag = man2.highmanag = man3.sname
dep = department ('STARTUP', [man1, man2, man3, man4])
print ('')
dep.add_team_members (man1, [dev1, dev2, des3])
dep.add_team_members (man2, [des1, des2, dev3])
dep.add_team_members (man3, '')     # check empty string adding 
dep.add_team_members (man3, [])     # check empty array adding
dep.add_team_members (man4, man3)   # check one manager, not array
dep.add_team_members (man4, dev1)   #   check one employee, not array
dep.add_team_members (man4, [man1,man2])   # check array of managers

# Departament structure printing
for m in dep.managlist:
    print ('\nManager: {} {} and his team:'.format(m.fname, m.sname))
    for e in m.team:
        #print (e.__class__.__name__)
        e.represent()

# Salary workflow process
print ('\nNow it`s salary time!')
for m in dep.managlist:
    #print ('Giving salary for ', m.__class__.__name__ )
    dep.givesalary(m)
    for e in m.team:
        if (e.__class__.__name__ ) != 'manager':
            #print ('Giving salary for ', e.__class__.__name__ )
            dep.givesalary(e)
